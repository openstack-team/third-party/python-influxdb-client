Source: python-influxdb-client
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper (>= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-aioresponses,
 python3-certifi,
 python3-dateutil,
 python3-httpretty,
 python3-jinja2,
 python3-pandas,
 python3-pluggy,
 python3-psutil,
 python3-py,
 python3-pytest,
 python3-pytest-timeout,
 python3-rx,
 python3-sphinx-rtd-theme,
 python3-urllib3,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-influxdb-client
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-influxdb-client.git
Homepage: https://github.com/influxdata/influxdb-client-python

Package: python3-influxdb-client
Architecture: all
Depends:
 python3-certifi,
 python3-dateutil,
 python3-pandas,
 python3-rx,
 python3-urllib3,
 ${misc:Depends},
 ${python3:Depends},
Description: InfluxDB 2.0 Python client library
 Client library for use with InfluxDB 2.x and Flux. InfluxDB 3.x users should
 instead use the lightweight v3 client library (influxdb3-python). InfluxDB 1.x
 users should use the v1 client library (influxdb-python). For ease of
 migration and a consistent query and write experience, v2 users should
 consider using InfluxQL and the v1 client library (influxdb-python).
 .
 The API of the influxdb-client is not the backwards-compatible with the old
 one influxdb-python.
